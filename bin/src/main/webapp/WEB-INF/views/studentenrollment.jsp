<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <style>
  .btn-link{
  border:none;
  outline:none;
  background:none;
  cursor:pointer;
  color:#0000EE;
  padding:0;
  text-decoration:underline;
  font-family:inherit;
  font-size:inherit;
}  
  </style>
<title>Student Home Page</title>
</head>

<body>
	<%@ include file="/WEB-INF/views/studentnavbar.jsp" %>
	
	<div class="container col-xs-12 col-md-12" >
		<h2>Enrollment</h2>
		<div class="panel-primary">
			<div class="panel-heading">Unit List</div>
			<div class="panel-body">
				<table class="table table-striped" id="list">
						<thead>
							<tr>
								<th class="sorting">UnitCode&Name</th>
								
				 			</tr>
						</thead>
						<tbody>
						<form action="studenthomepage" method="post">
							<input type="hidden" class="" name="studentid" value="${model.id}">
							<c:forEach var="unit" items="${model.units}">
								<tr>
									<td>								
									<button type="submit" name="" value="your_value" class="btn-link">${unit.code}  ${unit.name}</button>										
									</td>																
								</tr>
							</c:forEach>
							</form>
						</tbody>						
					</table>
			</div>
			
		</div>
	</div>
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
</body>

</html>
