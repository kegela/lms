<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<!DOCTYPE html>
<html lang="en">
<head>
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style>
		body{
			padding: 10px;
		}
		.error{
			color:red;
		}
	</style>
</head>
<body>
	<h1>Teacher Log In</h1> 
	<p class="error">${model.loginError}</p>
	<form class="row" action="teacherlogin" method="post" >
		
		<div class="form-group col-xs-12">
		    <label>TeacherID:</label>
		    <input type="text" class="form-control" name="id">
		</div>
		
		<div class="form-group col-xs-12">
		    <label>Password:</label>
		    <input type="password" class="form-control" name="password">
		</div>
		
		<div class="form-group col-xs-12">
			<input type="submit" class="btn btn-success" value="login"/>
		</div>

			
	</form>

</body>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
</html>