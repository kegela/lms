<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <style>
  .btn-link{
  border:none;
  outline:none;
  background:none;
  cursor:pointer;
  color:#0000EE;
  padding:0;
  text-decoration:underline;
  font-family:inherit;
  font-size:inherit;
  }  
  body{
  padding:10px;
  }
  </style>
<title>Admin User Management Page</title>
</head>

<body>
	<%@ include file="/WEB-INF/views/admin_navbar.jsp" %>
	<h1>User Management Page</h1>
	
	<div class="container col-xs-12 col-md-5" >
	<h3>Student list</h3>
		<div class="panel-primary">
			<div class="panel-heading">Student list</div>
			<div class="panel-body">
				<table class="table table-striped" id="list">
					<thead>
						<tr>
							<th class="sorting">StudentId&Name</th>								
			 			</tr>
					</thead>
					<tbody>
						<c:forEach var="student" items="${model.students}">
							<tr>
								<td>	
								<form action="/lms/deletestudent" method="POST">								
									<input type="hidden" class="form-control" name="studentId" value="${student.id}">
									<a href="adminstudentpage/${student.id}">${student.id}  ${student.name}</a>
									<input type="submit" class="btn-sm btn btn-primary" value="delete">
								</form>																																												
								
								</td>																
							</tr>
						</c:forEach>						
					</tbody>					
				</table>
			</div>
			</div>
		</div>
		
		<div class="container col-xs-12 col-md-5" >
		<h3>Teacher list</h3>
		<div class="panel-primary">
			<div class="panel-heading">Teacher list</div>
			<div class="panel-body">
				<table class="table table-striped" id="list">
					<thead>
						<tr>
							<th class="sorting">TeacherId&Name</th>								
			 			</tr>
					</thead>
					<tbody>
						<c:forEach var="teacher" items="${model.teachers}">
							<tr>
								<td>
								<form action="/lms/deleteteacher" method="POST">								
									<input type="hidden" class="form-control" name="teacherId" value="${teacher.id}">
									<a href="adminteacherpage/${teacher.id}">${teacher.id}  ${teacher.name}</a>
									<input type="submit" class="btn-sm btn btn-primary" value="delete">
								</form>						
								</td>																
							</tr>
						</c:forEach>						
					</tbody>					
				</table>
			</div>
			</div>
		</div>
		
		<div class="container col-xs-12 col-md-2">
			<br><br><br>
			<form class="row" action="adminaddstudent" method="get" enctype="multipart/form-data">
			<input type="submit" class="btn btn-primary" value="Add Student"/>
			</form>		
			<br>
			<form class="row" action="adminaddteacher" method="get" enctype="multipart/form-data">
			<input type="submit" class="btn btn-primary" value="Add Teacher"/>
			</form>
		</div>
		
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
</body>

</html>
