<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <style>
  .btn-link{
  border:none;
  outline:none;
  background:none;
  cursor:pointer;
  color:#0000EE;
  padding:0;
  text-decoration:underline;
  font-family:inherit;
  font-size:inherit;
  }  
  </style>
<title>Student Timetable Page</title>
</head>
<body>
	<%@ include file="/WEB-INF/views/student_navbar.jsp" %>
	<div class="container col-xs-12 col-md-6 col-md-offset-3" >
	<h2>Student Timetable</h2>
		<div class="panel-primary">
			<div class="panel-heading">Unit Timetable</div>
			<div class="panel-body">
				<table class="table table-striped" id="list">
					<thead>
						<tr>
							<th class="sorting">UnitCode</th>	
							<th class="sorting">UnitName</th>	
							<th class="sorting">Time</th>								
			 			</tr>
					</thead>
					<tbody>
						<c:forEach var="unit" items="${model.units}">
							<tr>
								<td>${unit.code}</td>
								<td>${unit.name}</td>
								<td>${unit.time}</td>																							
							</tr>
						</c:forEach>
					</tbody>					
				</table>
			</div>	
			<div class="panel-footer">Unit Timetable</div>		
			</div>					
		</div>		
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
</body>

</html>
