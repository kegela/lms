<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
<title>Unit information</title>
</head>
<body>
	<%@ include file="/WEB-INF/views/admin_navbar.jsp" %>
	<div class="container col-xs-12 col-md-9" >
	<h2>Teacher information</h2>
		
	<p><b>Teacher id:</b> ${model.id}</p>
	<p><b>Teacher Name:</b> ${model.name}</p>
	<p><b>Teacher Email:</b> ${model.email}</p>
	<p><b>Teacher Title:</b> ${model.title}</p>
	<p><b>Teacher Faculty:</b> ${model.faculty}</p>
	
 	<br>
 	<a href="/lms/adminusermanagement" class="btn btn-primary">Back</a>
 	</div>	

	<div class="container col-xs-12 col-md-3" >
		<br><br><br>
		<a href="/lms/admineditunit/${model.code}" class="btn btn-primary">Edit Student</a>
		<br><br>	
		
		<form class="row" action="/lms/admindelete" method="post" enctype="multipart/form-data">
			<input type="hidden" value="${model.code}" name="code">
			<input type="submit" class="btn btn-primary" value="Delete Student"/>
		</form>
	</div>
		
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
</body>

</html>
