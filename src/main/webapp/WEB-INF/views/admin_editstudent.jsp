<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
<title>Admin Add Student Page</title>
</head>
<body>
	<%@ include file="/WEB-INF/views/admin_navbar.jsp" %>
	<div class="container col-xs-12 col-md-9" >
	<h2>Student Editing</h2>		
		
		<c:choose>
		    <c:when test="${model.mode=='add'}">
		    <form class="row" action="/lms/adminaddstudent" method="post" enctype="multipart/form-data">
		        <div class="form-group col-xs-12">
		    	<label>Student ID:</label>
		    	<input type="text" class="form-control" name="id" >
				</div>			        
		    </c:when>    
		    <c:otherwise>
		    <form class="row" action="/lms/adminunitpage" method="post" enctype="multipart/form-data">
		        <div class="form-group col-xs-12">
		    	<label>Student ID:</label>
		    	<input type="text" class="form-control" name="id" value="${model.id}" readonly>
				</div>
		    </c:otherwise>
		</c:choose>
		
		<div class="form-group col-xs-12">
		    <label>Student Name:</label>
		    <input type="text" class="form-control" name="name" value="${model.name}">
		</div>	
		<div class="form-group col-xs-12">
		    <label>Student Password:</label>
		    <input type="text" class="form-control" name="password" value="${model.password}">
		</div>	
		<div class="form-group col-xs-12">
			<label>Student Email:</label>
			<input type="text" class="form-control" name="email" value="${model.email}">
		</div>			
		<div class="form-group col-xs-12">
		    <label>Student Major:</label>
		    <input type="text" class="form-control" name="major" value="${model.major}">
		</div>	
		<div class="form-group col-xs-12">
			<label>Student Faculty:</label>
			<input type="text" class="form-control" name="faculty" value="${model.faculty}">
		</div>		
				
		
		<div class="form-group col-xs-12">
			<input type="submit" class="btn btn-primary"/>
		</div>
		</form>		
		</div>			
		
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
</body>

</html>
