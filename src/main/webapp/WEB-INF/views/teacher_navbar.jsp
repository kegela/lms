


<nav class="navbar navbar-static-top navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">LMS</a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="teacherhomepage?teacherid=${model.teacherid}">My unit</a></li>
              
              <li><a href="teachertimetable?teacherid=${model.teacherid}">Timetable</a></li>          
            </ul>           
            <ul class="nav navbar-nav pull-right">
            <li><a href="<c:url value="login.htm"/>">LogOut</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </nav>