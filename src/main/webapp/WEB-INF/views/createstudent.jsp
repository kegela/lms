<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
	<title>Admin</title>
	<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
	<style>
		body{
			padding: 10px;
		}
	</style>
	<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
	<script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>	
</head>
<body>
	<form class="row" action="createstudent" method="post">
		<div class="form-group  col-xs-12">
		    <label>Id:</label>
		    <input type="text" class="form-control" name="id">
		</div>
		<div class="form-group col-xs-12">
		    <label>Name:</label>
		    <input type="text" class="form-control" name="name">
		</div>
		<div class="form-group col-xs-12">
		    <label>Password:</label>
		    <input type="password" class="form-control" name="password">
		</div>
		<div class="form-group col-xs-12">
			<label>Email:</label>
			<input type="email" class="form-control" name="email">
		</div>
		<div class="form-group col-xs-12">
			<input type="submit" class="btn btn-success"/>
		</div>
	</form>
</body>
</html>