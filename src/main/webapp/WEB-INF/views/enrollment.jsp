<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  
  <style>
  .btn-link{
  border:none;
  outline:none;
  background:none;
  cursor:pointer;
  color:#0000EE;
  padding:0;
  text-decoration:underline;
  font-family:inherit;
  font-size:inherit;
}
  
  </style>
<title>Insert title here</title>
</head>
<body>
<%@ include file="/WEB-INF/views/student_navbar.jsp" %>
<div class="" style="overflow:hidden">
	<div class="container col-xs-12 col-md-6" style="margin-bottom:-10000px; padding-bottom:10000px;">
				<form action="enrollment" id="commentForm" autocomplete="off" method="post">
				<h2>Enrollment</h2>
				<div  class="panel-primary">
				<div class="panel-heading">Choose Units</div>
				<div class="panel-body">
				
					<table class="table table-striped" id="list">
						
						<tbody>				
						
						<input type="hidden" class="" name="studentid" value="${model.studentid}">
							<c:forEach var="unit" items="${model.units}">
								<tr>
									<td>								
																		
										
									 <input type="radio" name="chooseunit" value="${unit.code}">${unit.code} ${unit.name}								
									
									</td>
																
								</tr>
							</c:forEach> 

						</tbody>
						
					</table>
				
				</div>
				
				<div class="panel-footer">Choose Units</div>
				</div>

				<div class="form-group">
					
					<input type="submit" class="form-control btn btn-success" value="ADD">
				</div>
			    
			</form>

	</div>



	<div class="container col-xs-12 col-md-6" style="margin-bottom:-10000px; padding-bottom:10000px;">
	<form action="enrollmentdelete" id="commentForm" autocomplete="off" method="post">
		<h2>Selected Units</h2>
		<div class="panel-primary">
			<div class="panel-heading">My Units</div>
			<div class="panel-body">
			
				<table class="table table-striped" id="list1">
						
						<tbody>				
						
						<input type="hidden" class="" name="studentid" value="${model.studentid}">
							<c:forEach var="unit1" items="${model.selectedunits}">
								<tr>
									<td>								
																		
										
									 <input type="radio" name="selectedunit" value="${unit1.code}">${unit1.code} ${unit1.name}								
									
									</td>
																
								</tr>
							</c:forEach> 
						
							
						
						</tbody>
						
					</table>
			
			</div>
			
		</div>
		<div class="panel-footer">My Units</div>
		
		
		
		<input type="submit" class="form-control btn btn-success" value="Delete">
		</form>
	</div>


</div>

<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
</body>
</html>