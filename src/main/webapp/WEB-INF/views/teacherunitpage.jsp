<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
  <style>
  .btn-link{
  border:none;
  outline:none;
  background:none;
  cursor:pointer;
  color:#0000EE;
  padding:0;
  text-decoration:underline;
  font-family:inherit;
  font-size:inherit;
}
  
  </style>
<title>Teacher Unit Page</title>
</head>
<body>
<%@ include file="/WEB-INF/views/teacher_navbar.jsp" %>
	<div class="col-md-4"></div>
	<div class="container col-xs-12 col-md-4" >
		<h2>Units Items</h2>
		<div class="panel-primary">
			<div class="panel-heading">Unit Items</div>
			<div class="panel-body">
				<table class="table table-striped" id="list">						
						<tbody>					
							<input type="hidden" class="" name="studentid" value="${model.teacherid}">							
								<tr>
									<td>									
									<a href="teacherannouncement?unitcode=${model.unitcode}&teacherid=${model.teacherid}">Announcement</a>									
									</td>																
								</tr>
								<tr>
									<td>
									<a href="teacherassignment?unitcode=${model.unitcode}&teacherid=${model.teacherid}">Assignment</a>
<!-- 									<button type="submit" name="announcement" value="your_value" class="btn-link">Announcement</button>										
 -->									</td>																
								</tr>
								<tr>
									<td>
									<a href="teachermaterial?unitcode=${model.unitcode}&teacherid=${model.teacherid}">Material</a>
									
									<!-- <button type="submit" name="material" value="your_value" class="btn-link"> Material</button>			-->							
									</td>
																
								</tr>
								<tr>
									<td>
									<a href="teacheremail?unitcode=${model.unitcode}&teacherid=${model.teacherid}">Email</a>
									
									<!-- <button type="submit" name="material" value="your_value" class="btn-link"> Material</button>			-->							
									</td>																
								</tr>							
						</tbody>						
					</table>
			</div>
			
		</div>
		<div class="panel-footer">Units Items</div>
	</div>
	<div class="col-md-4"></div>
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
</body>
</html>