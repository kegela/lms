<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
<title>Student information</title>
</head>
<body>
	<%@ include file="/WEB-INF/views/admin_navbar.jsp" %>
	<div class="container col-xs-12 col-md-9" >
	<h2>Student information</h2>
		
	<p><b>Student id:</b> ${model.id}</p>
	<p><b>Student Name:</b> ${model.name}</p>
	<p><b>Student Email:</b> ${model.email}</p>
	<p><b>Student Birth:</b> ${model.birth}</p>
	<p><b>Student Major:</b> ${model.major}</p>
	<p><b>Student Faculty:</b> ${model.faculty}</p>

	
 	<br>
 	<a href="/lms/adminusermanagement" class="btn btn-primary">Back</a>
 	</div>	

	<div class="container col-xs-12 col-md-3" >
		<br><br><br>
		<a href="/lms/admineditstudent/${model.id}" class="btn btn-primary">Edit Student</a>
		<br><br>	
	
	</div>
		
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
</body>

</html>
