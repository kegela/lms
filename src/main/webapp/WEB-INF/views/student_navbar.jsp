<nav class="navbar navbar-static-top navbar-default">
        <div class="container-fluid">
          <div class="navbar-header">
            <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target=".navbar-collapse">
              <span class="sr-only">Toggle navigation</span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
              <span class="icon-bar"></span>
            </button>
            <a class="navbar-brand" href="#">LMS</a>
          </div>
          <div class="navbar-collapse collapse">
            <ul class="nav navbar-nav">
              <li><a href="studenthomepage?studentid=${model.studentid}">My unit</a></li>
              <li><a href="enrollment?studentid=${model.studentid}">Enrollment</a></li>
              <li><a href="studenttimetable?studentid=${model.studentid}">Timetable</a></li>          
            </ul>           
            <ul class="nav navbar-nav pull-right">
            <li><a href="<c:url value="login.htm"/>">LogOut</a></li>
            </ul>
          </div><!--/.nav-collapse -->
        </div>
      </nav>
   