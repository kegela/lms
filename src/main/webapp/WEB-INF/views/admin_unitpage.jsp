<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
  
<title>Unit information</title>
</head>
<body>
	<%@ include file="/WEB-INF/views/admin_navbar.jsp" %>
	<div class="container col-xs-12 col-md-9" >
	<h2>Unit information</h2>
		
	<p><b>Unit Code:</b> ${model.code}</p>
	<p><b>Unit Name:</b> ${model.name}</p>
	<p><b>Unit Time:</b> ${model.time}</p>
	<p><b>Unit Description:</b> ${model.description}</p>

			
				
	<div class="container col-xs-12 col-md-4" >
	<table class="table table-striped" id="list">
		<thead>
			<tr>
				<th class="sorting">Student list: </th>		
				<th class="sorting">Delete </th>							
 			</tr>
		</thead>
		<tbody>
			<c:forEach var="student" items="${model.students}">
				<tr>
					<td>																																						
					<form action="/lms/deletestudentfromuni" method="POST">
						<input type="hidden" name="unitCode" value="${model.code}">
						<input type="hidden" class="form-control" name="studentId" value="${student.id}">
						${student.id}  ${student.name}
						<input type="submit" class="btn-sm btn btn-primary" value="delete">
					</form>
					</td>
																					
				</tr>
			</c:forEach>
		</tbody>					
	</table>
	<form action="/lms/addstudenttouni" method="POST">
		<input type="hidden" name="unitCode" value="${model.code}">
		<input style="width:150px; display:inline-block;" type="text" class="form-control" name="studentId" > &nbsp;
		<input type="submit" class="btn-sm btn btn-primary" value="add student">
	</form>
	</div>
	
	<div class="container col-xs-12 col-md-4 col-md-offset-1" >
	<table class="table table-striped" id="list">
		<thead>
			<tr>
				<th class="sorting">Teacher list:</th>	
				<th class="sorting">Delete </th>							
 			</tr>
		</thead>
		<tbody>
			<c:forEach var="teacher" items="${model.teachers}">
				<tr>
					<td>	
					<form action="/lms/deleteteacherfromuni" method="POST">
						${teacher.id}  ${teacher.name}
						<input type="hidden" name="unitCode" value="${model.code}">
						<input type="hidden" class="form-control" name="teacherId" value="${teacher.id}"> &nbsp;
						<input type="submit" class="btn-sm btn btn-primary" value="delete">
					</form>
					</td>	
																				
				</tr>
			</c:forEach>
		</tbody>					
	</table>
	<form action="/lms/addteachertouni" method="POST">
		<input type="hidden" name="unitCode" value="${model.code}">
		<input style="width:150px; display:inline-block;" type="text" class="form-control" name="teacherId" > &nbsp;
		<input type="submit" class="btn-sm btn btn-primary" value="add teacher">
	</form>
	</div>
	
 	
 	</div>	

	<div class="container col-xs-12 col-md-3" >
		<br><br><br>
		<a href="/lms/admineditunit/${model.code}" class="btn btn-primary pull-left">Edit Unit</a>
		<br><br><br>
		
		<form class="row" action="/lms/admindelete" method="post" enctype="multipart/form-data">
			<input type="hidden" value="${model.code}" name="code">
			<input type="submit" class="btn btn-primary pull-left" value="Delete Unit"/>
		</form>
		
		<br>
 		<a href="/lms/adminhomepage" class="btn btn-primary pull-left">Back</a>
	</div>
		
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
</body>

</html>
