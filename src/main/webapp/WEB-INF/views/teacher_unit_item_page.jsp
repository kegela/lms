<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<%@ include file="/WEB-INF/views/include.jsp" %>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<%@ taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core"%>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt"%>
<!DOCTYPE html PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/css/bootstrap.min.css">
  <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.5/css/bootstrap-theme.min.css">
  <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
  <script src="https://maxcdn.bootstrapcdn.com/bootstrap/3.3.7/js/bootstrap.min.js"></script>
 
<body>
<%@ include file="/WEB-INF/views/teacher_navbar.jsp" %>

	<div class="col-md-4"></div>
	

	<div class="container col-xs-12 col-md-4" >
		<h2>${model.item}</h2>
		<div class="panel-primary">
			<div class="panel-heading">${model.unitcode} ${model.item}</div>
			<div class="panel-body">
				<table class="table table-striped" id="list">
						
						<tbody>
					
							<%-- <input type="hidden" class="" name="teacherid" value="${model.id}"> --%>
							
							<c:forEach var="itemTemp" items="${model.list}">
								<tr>
									<td>								
																		
										
									<!--  <input type="hidden" class="" name="studentid" value="${model.id}">-->				
								<%-- 	<button type="submit" name="unitcode" value="${unit.code}" class="btn-link">${unit.code}  ${unit.name}</button>	 --%>									
									 <a href="teacheritemdetailpage?unitcode=${model.unitcode}&teacherid=${model.teacherid}&item=${model.item}&itemid=${itemTemp.id}">${model.unitcode} ${itemTemp.title}</a>
									 
									<%-- <a href= "${itemTemp.filepath}" > ${itemTemp.title}</a> --%>
									</td>
																
								</tr>
							</c:forEach>
						
						</tbody>
						
					</table>
			</div>
			<div class="panel-footer">${model.unitcode} ${model.item}</div>
			
		</div>
	</div>

	<div class="col-md-4"></div>
	<%@ include file="/WEB-INF/views/bootstrap_jquery.jsp" %>
</body>
</html>