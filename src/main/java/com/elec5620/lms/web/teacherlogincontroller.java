package com.elec5620.lms.web;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Teacher;
import com.elec5620.lms.domain.Unit;
import com.elec5620.lms.service.ITeacherService;
import com.elec5620.lms.service.IUnitService;
import com.elec5620.lms.util.Base64Util;


/*
 * Login profile - by Leo
 * */
@Controller
public class teacherlogincontroller {

	
	
	@Resource(name="teacherService")
	private ITeacherService teacherService;
	@Resource(name="unitService")
	private IUnitService unitService;

	
	@RequestMapping(value ="/teacherlogin",method = RequestMethod.GET)
	public ModelAndView logInPage(){
		Map<String, Object> model = new HashMap<String, Object>();
		
        return new ModelAndView("teacherlogin", "model", model);
	}
	
//	@RequestMapping(value="/teacherlogin",method=RequestMethod.POST)
//	public ModelAndView verifyLogin(@RequestParam("id") long id, @RequestParam("password") String password, 
//			HttpSession session, Model model) {
//		Teacher teacher = TeacherService.loginUser(id, password);
//		if(teacher == null){
//			model.addAttribute("loginError", "User name and password don't match, please try again!");
//			return new ModelAndView("teacherlogin", "model", model);
//		}
//		session.setAttribute("loggedUnUser", teacher);
//		return new ModelAndView("redirect:/home", "model", model);
//}
	
	@RequestMapping(value="/teacherlogin",method=RequestMethod.POST)
	public ModelAndView verifyLogin(@RequestParam("id") long id, @RequestParam("password") String password){
		
			Map<String, Object> model = new HashMap<String, Object>();
			
			Teacher thr=teacherService.getTeacherById(id);
			if(thr.getPassword().equals(password))
			{			
				Set<Unit> units=unitService.getTeacherUnits(id);
				model.put("name",thr.getName() );
				model.put("email", thr.getEmail());
				model.put("title", thr.getTitle());
				model.put("faculty", thr.getFaculty());
				model.put("gender", thr.getGender());
				model.put("teacherid", id);
				model.put("units", units);
				 return new ModelAndView("teacherhomepage", "model", model);
			}
			else{
				 return new ModelAndView("teacherlogin", "model", model);
			}
		
			
	        
	
	
	}
	

	

}
