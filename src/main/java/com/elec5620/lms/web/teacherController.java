package com.elec5620.lms.web;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.elec5620.lms.util.SendEmailUtil;
import com.elec5620.lms.domain.Announcement;
import com.elec5620.lms.domain.Assignment;
import com.elec5620.lms.domain.Material;
import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Teacher;
import com.elec5620.lms.domain.Unit;
import com.elec5620.lms.service.IAnnouncementService;
import com.elec5620.lms.service.IAssignmentService;
import com.elec5620.lms.service.IMaterialService;
import com.elec5620.lms.service.IStudentService;
import com.elec5620.lms.service.ITeacherService;
import com.elec5620.lms.service.IUnitService;
import com.elec5620.lms.service.IUnitService;
import com.elec5620.lms.util.Base64Util;


@Controller
public class teacherController {
	
	@Resource(name="studentService")
	private IStudentService studentService;
	@Resource(name="teacherService")
	private ITeacherService teacherService;
	@Resource(name="unitService")
	private IUnitService unitService;
	@Resource(name="assignmentService")
	private IAssignmentService assignmentService;
	@Resource(name="announcementService")
	private IAnnouncementService announcementService;
	@Resource(name="materialService")
	private IMaterialService materialService;
	
	@RequestMapping(value ="/teacherhomepage",method = RequestMethod.GET)
	public ModelAndView logInPage(@RequestParam("teacherid") long teacherid){
		Map<String, Object> model = new HashMap<String, Object>();
		Teacher teacher=teacherService.getTeacherById(teacherid);
		List<Unit> units = new ArrayList<Unit>(teacher.getUnits());
		
		model.put("name",teacher.getName() );
		model.put("email", teacher.getEmail());
		
		model.put("faculty", teacher.getFaculty());
		model.put("gender", teacher.getGender());
		model.put("teacherid", teacherid);
		model.put("title",teacher.getTitle() );
		model.put("units", units);
		
        return new ModelAndView("teacherhomepage", "model", model);
	}
	
	@RequestMapping(value ="/teacherunitpage",method = RequestMethod.GET)
	public ModelAndView studentUnitPage(@RequestParam("unitcode") String unitcode,@RequestParam("teacherid") long teacherid ){
		//获取数据库信息
		Map<String, Object> model = new HashMap<String, Object>();
//		System.out.println("unitcode="+unitcode);
//		System.out.println("studentid="+studentid);
		model.put("teacherid", teacherid);
		model.put("unitcode", unitcode);
		
		return new ModelAndView("teacherunitpage", "model", model);     
	}
	
	
	@RequestMapping(value ="/teacherassignment",method = RequestMethod.GET)
	public ModelAndView teacherassignmentlistPage(@RequestParam("unitcode") String unitcode,@RequestParam("teacherid") long teacherid){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("unitcode", unitcode);
		model.put("teacherid", teacherid);
		model.put("item", "assignment");
		Set<Assignment> assignments=assignmentService.getAssignments(unitcode);
		model.put("list",assignments);
        return new ModelAndView("teacher_unit_item_page", "model", model);
	}
	
	@RequestMapping(value ="/teacherannouncement",method = RequestMethod.GET)
	public ModelAndView announcementlistPage(@RequestParam("unitcode") String unitcode,@RequestParam("teacherid") long teacherid){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("unitcode", unitcode);
		model.put("teacherid", teacherid);
		model.put("item", "announcement");
		Set<Announcement> announcements=announcementService.getAnnouncements(unitcode);
		model.put("list",announcements);
        return new ModelAndView("teacher_unit_item_page", "model", model);
	}
	
	@RequestMapping(value ="/teachermaterial",method = RequestMethod.GET)
	public ModelAndView materiallistPage(@RequestParam("unitcode") String unitcode,@RequestParam("teacherid") long teacherid){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("unitcode", unitcode);
		model.put("teacherid", teacherid);
		model.put("item", "material");
		
		Set<Material> materials=materialService.getMaterials(unitcode);
//		
		for(Material Mtemp:materials)
		{
			System.out.println("Material:title="+Mtemp.getTitle());
			
		}
		model.put("list",materials);
		
        return new ModelAndView("teacher_unit_item_page", "model", model);
	}
	
	@RequestMapping(value ="/teacheremail",method = RequestMethod.GET)
	public ModelAndView teacheremailPage(@RequestParam("unitcode") String unitcode,@RequestParam("teacherid") long teacherid){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("unitcode", unitcode);
		model.put("teacherid", teacherid);
//		model.put("item", "announcement");
//		Set<Announcement> announcements=announcementService.getAnnouncements(unitcode);
//		model.put("list",announcements);
        return new ModelAndView("teacherEmail", "model", model);
	}
	
	@RequestMapping(value ="/teacheremail",method = RequestMethod.POST)
	public ModelAndView teacheremailsendPage(@RequestParam("unitcode") String unitcode,@RequestParam("teacherid") long teacherid,@RequestParam("title") String title,@RequestParam("content") String content){
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("unitcode", unitcode);
		model.put("teacherid", teacherid);
		System.out.println("Send Post");
		System.out.println("title="+title);
		System.out.println("content="+content);
		Set <Student> stus= unitService.getUnitStudents(unitcode);
		for(Student tempStudent:stus)
		{
			SendEmailUtil send = new SendEmailUtil(tempStudent.getEmail(), unitService.getUnitByCode(unitcode).getCode()+": "+title, content);
			send.sendEmail();
			
		}
		
//		model.put("item", "announcement");
//		Set<Announcement> announcements=announcementService.getAnnouncements(unitcode);
//		model.put("list",announcements);
		
//		SendEmailUtil send = new SendEmailUtil("sche2145@icloud.com", "Feedme Info", "This message form feedme!");
//		send.sendEmail();
        return new ModelAndView("teacherEmail", "model", model);
	}
	
	@RequestMapping(value ="/teacheritemdetailpage",method = RequestMethod.GET)
	public ModelAndView tiemDetailPage(@RequestParam("unitcode") String unitcode,@RequestParam("teacherid") long teacherid,@RequestParam("item") String item,@RequestParam("itemid") long itemid){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("unitcode", unitcode);
		model.put("teacherid", teacherid);
		model.put("item", item);
		
		model.put("itemid", itemid);
		if(item.equals("assignment")){
			Assignment assignment = assignmentService.getAssignmentByid(itemid);
			model.put("itemtitle",assignment.getTitle());
			model.put("itemdescription", assignment.getDescription());
//			model.put("itemurl", assignment.getUrl());
			model.put("itemdetail",assignment);
			System.out.println("assignment");
		}
		else if(item.equals("announcement")){
			Announcement announcement = announcementService.getAnnouncementByid(itemid);
			model.put("itemtitle",announcement.getTitle());
			model.put("itemdescription", announcement.getDescription());
			System.out.println("announcement");
			model.put("itemdetail",announcement);
		}
		else if(item.equals("material")){
			Material material = materialService.getMaterialByid(itemid);
			model.put("itemtitle",material.getTitle());
			model.put("filepath", material.getFilepath());
//			model.put("itemurl", material.getUrl());
			//model.put("itemdescription", material.getDescription());
			System.out.println("material");
			//model.put("itemdetail",material);
		}
		//model.put("list",announcements);
        return new ModelAndView("teacher_itemdetailpage", "model", model);
	}
	
	@RequestMapping(value ="/teachertimetable",method = RequestMethod.GET)
	public ModelAndView teacherTimetablePage(@RequestParam("teacherid") long teacherid){
		Teacher teacher = teacherService.getTeacherById(teacherid);
		List<Unit> units = new ArrayList<Unit>(teacher.getUnits());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("teacherid", teacherid);
		model.put("units", units);	
        return new ModelAndView("teachertimetable", "model", model);
	}

}
