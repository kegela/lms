package com.elec5620.lms.web;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import javax.annotation.Resource;


import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;

import com.elec5620.lms.domain.Announcement;
import com.elec5620.lms.domain.Assignment;
import com.elec5620.lms.domain.Material;
import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Unit;
import com.elec5620.lms.service.IAnnouncementService;
import com.elec5620.lms.service.IAssignmentService;
import com.elec5620.lms.service.IMaterialService;
import com.elec5620.lms.service.IStudentService;
import com.elec5620.lms.service.IUnitService;
import com.elec5620.lms.util.Base64Util;

/*
 * 
 * */
@Controller
public class StudentController {
	
	@Resource(name="studentService")
	private IStudentService studentService;
	@Resource(name="unitService")
	private IUnitService unitService;
	@Resource(name="assignmentService")
	private IAssignmentService assignmentService;
	@Resource(name="announcementService")
	private IAnnouncementService announcementService;
	@Resource(name="materialService")
	private IMaterialService materialService;
	
	
//	@Resource(name="studentService")
//	private IStudentService studentService;
	
	@RequestMapping(value ="/studenthomepage",method = RequestMethod.GET)
	public ModelAndView logInPage(@RequestParam("studentid") long studentid){
		Map<String, Object> model = new HashMap<String, Object>();
		Student student=studentService.getStudentById(studentid);
		
		//Set<Unit> units=unitService.getStudnetUnits(id);
		Set<Unit> units=unitService.getStudentUnits(studentid);
		model.put("name",student.getName() );
		model.put("email", student.getEmail());
		model.put("major", student.getMajor());
		model.put("faculty", student.getFaculty());
		model.put("gender", student.getGender());
		model.put("studentid", studentid);
		model.put("units", units);
        return new ModelAndView("studenthomepage", "model", model);
	}
	
	@RequestMapping(value ="/studenthomepage",method = RequestMethod.POST)
	public ModelAndView tounitPage(@RequestParam("studentid") long studentid){
		//获取数据库信息
		Set<Unit> units=unitService.getStudentUnits(studentid);
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("studentid", studentid);
		model.put("units", units);		
		return new ModelAndView("studentunitpage", "model", model);      
	}

	
	@RequestMapping(value ="/studentunitpage",method = RequestMethod.GET)
	public ModelAndView studentUnitPage(@RequestParam("unitcode") String unitcode,@RequestParam("studentid") long studentid ){
		//获取数据库信息
		Map<String, Object> model = new HashMap<String, Object>();
//		System.out.println("unitcode="+unitcode);
//		System.out.println("studentid="+studentid);
		model.put("studentid", studentid);
		model.put("unitcode", unitcode);		
		return new ModelAndView("studentunitpage", "model", model);       
	}
	
	@RequestMapping(value ="/assignment",method = RequestMethod.GET)
	public ModelAndView assignmentlistPage(@RequestParam("unitcode") String unitcode,@RequestParam("studentid") long studentid){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("unitcode", unitcode);
		model.put("studentid", studentid);
		model.put("item", "assignment");
		Set<Assignment> assignments=assignmentService.getAssignments(unitcode);
		model.put("list",assignments);
        return new ModelAndView("unit_item_page", "model", model);
	}
	
	@RequestMapping(value ="/announcement",method = RequestMethod.GET)
	public ModelAndView announcementlistPage(@RequestParam("unitcode") String unitcode,@RequestParam("studentid") long studentid){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("unitcode", unitcode);
		model.put("studentid", studentid);
		model.put("item", "announcement");
		Set<Announcement> announcements=announcementService.getAnnouncements(unitcode);
		model.put("list",announcements);
        return new ModelAndView("unit_item_page", "model", model);
	}
	
	@RequestMapping(value ="/material",method = RequestMethod.GET)
	public ModelAndView materiallistPage(@RequestParam("unitcode") String unitcode,@RequestParam("studentid") long studentid){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("unitcode", unitcode);
		model.put("studentid", studentid);
		model.put("item", "material");		
		Set<Material> materials=materialService.getMaterials(unitcode);		
		for(Material Mtemp:materials)
		{
			System.out.println("Material:title="+Mtemp.getTitle());			
		}
		model.put("list",materials);		
        return new ModelAndView("unit_item_page", "model", model);
	}
	
	@RequestMapping(value ="/itemdetailpage",method = RequestMethod.GET)
	public ModelAndView tiemDetailPage(@RequestParam("unitcode") String unitcode,@RequestParam("studentid") long studentid,@RequestParam("item") String item,@RequestParam("itemid") long itemid){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("unitcode", unitcode);
		model.put("studentid", studentid);
		model.put("item", item);
		
		model.put("itemid", itemid);
		if(item.equals("assignment")){
			Assignment assignment = assignmentService.getAssignmentByid(itemid);
			model.put("itemtitle",assignment.getTitle());
			model.put("filepath", assignment.getFilepath());
			model.put("itemdescription", assignment.getDescription());
//			model.put("itemurl", assignment.getUrl());
			model.put("itemdetail",assignment);
			System.out.println("assignment");
		}
		else if(item.equals("announcement")){
			Announcement announcement = announcementService.getAnnouncementByid(itemid);
			model.put("itemtitle",announcement.getTitle());
			model.put("itemdescription", announcement.getDescription());
			System.out.println("announcement");
			model.put("itemdetail",announcement);
		}
		else if(item.equals("material")){
			Material material = materialService.getMaterialByid(itemid);
			model.put("itemtitle",material.getTitle());
			model.put("filepath", material.getFilepath());
//			model.put("itemurl", material.getUrl());
			//model.put("itemdescription", material.getDescription());
			System.out.println("material");
			//model.put("itemdetail",material);
		
		}
		//model.put("list",announcements);
        return new ModelAndView("student_itemdetailpage", "model", model);
	}
	
	
	@RequestMapping(value ="/enrollment",method = RequestMethod.GET)
	public ModelAndView enrollmentPage(@RequestParam("studentid") long studentid){
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("studentid", studentid);
		//model.put("item", "material");
		List<Unit> units =unitService.listUnits();	
		Set<Unit> selectedunits=unitService.getStudentUnits(studentid);
	
//		List<Unit> sendunits =new ArrayList();
//		for(Unit tempunit:selectedunits){
//			for(Unit secondtempunit:units)
//			{
//				if(tempunit.getCode().equals(secondtempunit.getCode()))
//				{
//					
//					units.remove(secondtempunit);
//				}
//			
//			}
//			
//		}
		model.put("selectedunits", selectedunits);
		model.put("units", units);
        return new ModelAndView("enrollment", "model", model);
	}
	
	@RequestMapping(value ="/enrollment",method = RequestMethod.POST)
	public ModelAndView addenrollmentPage(@RequestParam("studentid") long studentid,@RequestParam("chooseunit") String unitcode){
		//System.out.println("============");
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("studentid", studentid);
		List<Unit> units=unitService.listUnits();
		model.put("units", units);		
		studentService.selectUnit(studentid, unitcode);
		Set<Unit> selectedunits=unitService.getStudentUnits(studentid);
		model.put("selectedunits", selectedunits);
        return new ModelAndView("enrollment", "model", model);
	}
	
	@RequestMapping(value ="/enrollmentdelete",method = RequestMethod.POST)
	public ModelAndView deleteenrollmentPage(@RequestParam("studentid") long studentid,@RequestParam("selectedunit") String unitcode){
		//System.out.println("++++++++");
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("studentid", studentid);
		List<Unit> units=unitService.listUnits();
		model.put("units", units);
		studentService.dropUnit(studentid, unitcode);		
		Set<Unit> selectedunits=unitService.getStudentUnits(studentid);
		model.put("selectedunits", selectedunits);
        return new ModelAndView("enrollment", "model", model);
	}
	
	@RequestMapping(value ="/studenttimetable",method = RequestMethod.GET)
	public ModelAndView studenttimetablePage(@RequestParam("studentid") long studentid){
		Student student = studentService.getStudentById(studentid);
		List<Unit> units = new ArrayList<Unit>(student.getUnits());
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("studentid", studentid);
		model.put("units", units);	
        return new ModelAndView("studenttimetable", "model", model);
	}
	
//	@RequestMapping(value ="/announcement/{unitcode}/{studentid}",method = RequestMethod.GET)
//	public ModelAndView listAnnouncementPage(@PathVariable("unitcode") String unitcode,@PathVariable("studentid") long studentid ){
//		//获取数据库信息
//		Map<String, Object> model = new HashMap<String, Object>();
//		model.put("studentid", studentid);
//		model.put("unitcode", unitcode);
//		Set<Announcement> announcements=announcementService.getAnnouncements(unitcode);
//		model.put("list",announcements);	
//		return new ModelAndView("unit_item_page", "model", model);     
//	}
	
//	@RequestMapping(value ="/studentunitpage/{unitcode}/{choise}/{studentid}",method = RequestMethod.GET)
//	public ModelAndView studentItemDetail(@PathVariable("unitcode") String unitcode,@PathVariable("choies") String choies,@PathVariable("studentid") long studentid ){
//		//获取数据库信息
//		Map<String, Object> model = new HashMap<String, Object>();
//		model.put("studentid", studentid);
//		model.put("unitcode", unitcode);
//		model.put("item", choies);
//		if(choies=="assignment")
//		{
//			Set<Assignment> assignments=assignmentService.getAssignments(unitcode);
//			model.put("assignment",assignments);
//		}
//		else if(choies=="annoucement")
//		{
//			Set<Announcement> announcements=announcementService.getAnnouncements(unitcode);
//			model.put("announcement",announcements);
//		}
//		else
//		{
//			Set<Material> materials=materialService.getMaterials(unitcode);
//			model.put("material",materials);
//		}		
//		return new ModelAndView("unit_item_page", "model", model);     
//       
//	}
	
//	@RequestMapping(value ="/studentunitpage",method = RequestMethod.POST)
//	public ModelAndView studentUnitPageGo(){
//		//获取数据库信息

//		Map<String, Object> model = new HashMap<String, Object>();
//		
//		return new ModelAndView("studentunitpage", "model", model);
//        
//       
//	}

	
	
}
