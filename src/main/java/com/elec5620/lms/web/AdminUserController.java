package com.elec5620.lms.web;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.ModelAndView;

import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Teacher;
import com.elec5620.lms.domain.Unit;
import com.elec5620.lms.service.IStudentService;
import com.elec5620.lms.service.ITeacherService;
import com.elec5620.lms.service.TeacherService;

@Controller
public class AdminUserController {

	@Resource(name="studentService")
	private IStudentService studentService;

	@Resource(name="teacherService")
	private ITeacherService teacherService;
	
	@RequestMapping(value ="/adminusermanagement",method = RequestMethod.GET)
	public ModelAndView listUnit(){
		List<Student> students = studentService.listStudents();
		List<Teacher> teachers = teacherService.listTeachers();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("students", students);
		model.put("teachers", teachers);
		return new ModelAndView("adminusermanagement", "model", model);
	}
	
	@RequestMapping(value ="/adminstudentpage/{student_id}",method = RequestMethod.GET)
	public ModelAndView viewStudent(@PathVariable("student_id") long student_id) throws IOException{
		Student student = studentService.getStudentById(student_id);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("id", student.getId());
		model.put("name", student.getName());
		model.put("email", student.getEmail());
		model.put("gender", student.getGender());
		model.put("birth", student.getDateofbirth());
		model.put("major", student.getMajor());
		model.put("faculty", student.getFaculty());
		model.put("enrollment_year", student.getEnrollment_year());		
		return new ModelAndView("admin_studentpage", "model", model);
	}
	
	@RequestMapping(value ="/adminaddstudent",method = RequestMethod.GET)
	public ModelAndView addStudentPage(){    
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mode", "add");
		return new ModelAndView("admin_editstudent", "model", model);
	}
	
	@RequestMapping(value ="/adminaddstudent",method = RequestMethod.POST)
	public ModelAndView addStudent(@RequestParam("id") long id, @RequestParam("name") String name,
			@RequestParam("password") String password,
			@RequestParam("email") String email, 
			@RequestParam("major") String major, 
			@RequestParam("faculty") String faculty
			) throws IOException{	
		
	    /* store student information in database*/
		Student student = new Student();
		student.setId(id);
		student.setName(name);
		student.setPassword(password);
		student.setEmail(email);
		student.setMajor(major);
		student.setFaculty(faculty);
		studentService.addStudent(student);
		
		/* display added unit information */
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("id", student.getId());
		model.put("name", student.getName());
		model.put("email", student.getEmail());
		model.put("major", student.getMajor());
		model.put("faculty", student.getFaculty());
		return new ModelAndView("admin_studentpage", "model", model);
	}
	
	@RequestMapping(value ="/admineditstudent/{id}",method = RequestMethod.GET)
	public ModelAndView editUnit(@PathVariable("id") long id ) throws IOException{
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mode", "edit");
		Student student = studentService.getStudentById(id);
		model.put("id", student.getId());
		model.put("name", student.getName());
		model.put("password", student.getPassword());
		model.put("email", student.getEmail());
		model.put("major", student.getMajor());
		model.put("faculty", student.getFaculty());
		return new ModelAndView("admin_editstudent", "model", model);
	}
	
	
	@RequestMapping(value ="/deletestudent",method = RequestMethod.POST)
	public String deleteStudent(@RequestParam("studentId") long studentId) throws IOException{
		studentService.deleteStudent(studentId);
		return "redirect:adminusermanagement";
	}
	
	@RequestMapping(value ="/deleteteacher",method = RequestMethod.POST)
	public String deleteTeacher(@RequestParam("teacherId") long teacherId) throws IOException{
		teacherService.deleteTeacher(teacherId);
		return "redirect:adminusermanagement";
	}
	
	@RequestMapping(value ="/adminteacherpage/{teacher_id}",method = RequestMethod.GET)
	public ModelAndView viewTeacher(@PathVariable("teacher_id") long teacher_id) throws IOException{
		Teacher teacher = teacherService.getTeacherById(teacher_id);
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("id", teacher.getId());
		model.put("name", teacher.getName());
		model.put("email", teacher.getEmail());
		model.put("gender", teacher.getGender());
		model.put("birth", teacher.getDateofbirth());
		model.put("title", teacher.getTitle());
		model.put("faculty", teacher.getFaculty());		
		return new ModelAndView("admin_teacherpage", "model", model);
	}
	
	
}
