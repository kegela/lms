package com.elec5620.lms.web;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Unit;
import com.elec5620.lms.service.IStudentService;
import com.elec5620.lms.service.IUnitService;
import com.elec5620.lms.util.Base64Util;


/*
 * Login profile - by Leo
 * */
@Controller
public class studentlogincontroller {

	@Resource(name="studentService")
	private IStudentService studentService;
	
	@Resource(name="unitService")
	private IUnitService unitService;

	
	@RequestMapping(value ="/studentlogin",method = RequestMethod.GET)
	public ModelAndView logInPage(){
		Map<String, Object> model = new HashMap<String, Object>();
		
        return new ModelAndView("studentlogin", "model", model);
	}
	
	@RequestMapping(value="/studentlogin",method=RequestMethod.POST)
	public ModelAndView verifyLogin(@RequestParam("id") long id, @RequestParam("password") String password){
		
			Map<String, Object> model = new HashMap<String, Object>();
			
			Student stu=studentService.getStudentById(id);
			if(stu.getPassword().equals(password))
			{
				model.put("studentid", id);
				
				Set<Unit> units=unitService.getStudentUnits(id);
				model.put("name",stu.getName() );
				model.put("email", stu.getEmail());
				model.put("major", stu.getMajor());
				model.put("faculty", stu.getFaculty());
				model.put("gender", stu.getGender());
				model.put("studentid", id);
				model.put("units", units);
				 return new ModelAndView("studenthomepage", "model", model);
			}
			else{
				 return new ModelAndView("studentlogin", "model", model);
			}

	}
	

}
