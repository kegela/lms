package com.elec5620.lms.web;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.UUID;

import javax.annotation.Resource;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.elec5620.lms.domain.Announcement;
import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Teacher;
import com.elec5620.lms.domain.Unit;
import com.elec5620.lms.service.AnnouncementService;
import com.elec5620.lms.service.IAnnouncementService;
import com.elec5620.lms.service.IStudentService;
import com.elec5620.lms.service.ITeacherService;
import com.elec5620.lms.service.IUnitService;

@Controller
public class AdminController {
	
	@Resource(name="teacherService")
	private ITeacherService teacherService;
	
	@Resource(name="studentService")
	private IStudentService studentService;
	
	@Resource(name="unitService")
	private IUnitService unitService;
	
	@Resource(name="announcementService")
	private IAnnouncementService announcementService;
	
	
	@RequestMapping(value ="/adminhomepage",method = RequestMethod.GET)
	public ModelAndView listUnit(){
		List<Unit> units = unitService.listUnits();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("units", units);
		return new ModelAndView("adminhomepage", "model", model);
	}
	
	@RequestMapping(value ="/adminhomepage",method = RequestMethod.POST)
	public ModelAndView deleteUnit(@RequestParam("code") String code)throws IOException{
		Unit unit = unitService.getUnitByCode(code);
		if(unit.getStudents().size()==0 && unit.getTeachers().size()==0){
			unitService.deleteUnit(unit);
			List<Unit> units = unitService.listUnits();
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("units", units);
			return new ModelAndView("adminhomepage", "model", model);
		}
		else{
			Map<String, Object> model = new HashMap<String, Object>();
			model.put("code", code);
			return new ModelAndView("admin_errordelete", "model", model);
		}
	}
	
	@RequestMapping(value ="/adminaddunit",method = RequestMethod.GET)
	public ModelAndView addUnitPage(){    
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mode", "add");
		return new ModelAndView("admin_editunit", "model", model);
	}
	
	@RequestMapping(value ="/adminaddunit",method = RequestMethod.POST)
	public ModelAndView addUnit(@RequestParam("code") String code, @RequestParam("name") String name,
			@RequestParam("time") String time,@RequestParam("description") String description) throws IOException{
		
	    /* store unit information in database*/
		Unit unit = new Unit();
		unit.setCode(code);
		unit.setName(name);
		unit.setTime(time);
		unit.setDescription(description);
		unitService.addUnit(unit);
		
		/* display added unit information */
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("code", unit.getCode());
		model.put("name", unit.getName());
		model.put("time", unit.getTime());
		model.put("description", unit.getDescription());
		return new ModelAndView("admin_unitpage", "model", model);
	}
	
	@RequestMapping(value ="/adminunitpage/{unit_code}",method = RequestMethod.GET)
	public ModelAndView viewUnit(@PathVariable("unit_code") String unit_code) throws IOException{
		Map<String, Object> model = new HashMap<String, Object>();
		Unit unit = unitService.getUnitByCode(unit_code);
		List<Student> students = new ArrayList<Student>(unit.getStudents());
		List<Teacher> teachers = new ArrayList<Teacher>(unit.getTeachers());
		model.put("code", unit.getCode());
		model.put("name", unit.getName());
		model.put("description", unit.getDescription());
		model.put("time", unit.getTime());
		model.put("students", students);
		model.put("teachers", teachers);
		return new ModelAndView("admin_unitpage", "model", model);
	}
	
	@RequestMapping(value ="/addstudenttouni",method = RequestMethod.POST)
	public String addStudent(@RequestParam("studentId") long studentId, @RequestParam("unitCode") String unitCode) throws IOException{
		studentService.selectUnit(studentId, unitCode);
		return "redirect:adminunitpage/"+unitCode;
	}
	
	@RequestMapping(value ="/deletestudentfromuni",method = RequestMethod.POST)
	public String deleteStudentFromUni(@RequestParam("studentId") long studentId, @RequestParam("unitCode") String unitCode) throws IOException{
		studentService.dropUnit(studentId, unitCode);
		return "redirect:adminunitpage/"+unitCode;
	}
	
	@RequestMapping(value ="/addteachertouni",method = RequestMethod.POST)
	public String addTeacherToUni(@RequestParam("teacherId") long teacherId, @RequestParam("unitCode") String unitCode) throws IOException{
		teacherService.selectUnit(teacherId, unitCode);
		return "redirect:adminunitpage/"+unitCode;
	}
	
	@RequestMapping(value ="/deleteteacherfromuni",method = RequestMethod.POST)
	public String deleteteacherfromuni(@RequestParam("teacherId") long teacherId, @RequestParam("unitCode") String unitCode) throws IOException{
		teacherService.dropUnit(teacherId, unitCode);
		return "redirect:adminunitpage/"+unitCode;
	}
	
	
	@RequestMapping(value ="/adminpublishannoun",method = RequestMethod.GET)
	public String publishAnnoun(){     
        return "admin_publishannoun";
	}
	
	@RequestMapping(value ="/adminpublishannoun",method = RequestMethod.POST)
	public ModelAndView announcementPage(@RequestParam("title") String title, @RequestParam("unit_code") String unit_code,
			@RequestParam("description") String description) throws IOException{
		
	    /* store unit information in database*/
		Announcement announcement = new Announcement();
		announcement.setTitle(title);
		announcement.setUnit(unitService.getUnitByCode(unit_code));
		announcement.setDescription(description);
		announcementService.addAnnouncement(announcement);
		
		/* display added unit information */
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("title", announcement.getTitle());
		model.put("unit_code", announcement.getUnit().getCode());
		model.put("description", announcement.getDescription());
		return new ModelAndView("admin_announpage", "model", model);
	}
	
	@RequestMapping(value ="/admineditunit/{code}",method = RequestMethod.GET)
	public ModelAndView editUnit(@PathVariable("code") String code ) throws IOException{
		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("mode", "edit");
		Unit unit = unitService.getUnitByCode(code);
		model.put("code", unit.getCode());
		model.put("name", unit.getName());
		model.put("time", unit.getTime());
		model.put("description", unit.getDescription());
		return new ModelAndView("admin_editunit", "model", model);
	}
	
	@RequestMapping(value ="/adminunitpage",method = RequestMethod.POST)
	public ModelAndView updateUnit(@RequestParam("code") String code, @RequestParam("name") String name,
			@RequestParam("time") String time,@RequestParam("description") String description) throws IOException{
		
	    /* update unit information in database*/
		Unit unit = new Unit();
		unit.setCode(code);
		unit.setName(name);
		unit.setTime(time);
		unit.setDescription(description);
		unitService.updateUnit(unit);
		
		/* display added unit information */
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("code", unit.getCode());
		model.put("name", unit.getName());
		model.put("time", unit.getTime());
		model.put("description", unit.getDescription());
		return new ModelAndView("admin_unitpage", "model", model);
	}
	
	@RequestMapping(value ="/admindelete",method = RequestMethod.POST)
	public ModelAndView deleteUnitPage(@RequestParam("code") String code ) throws IOException{		
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("code", code);
		return new ModelAndView("admin_deletepage", "model", model);
	}
	
	
	
	
	
	
	@RequestMapping(value ="/usermanagement",method = RequestMethod.GET)
	public ModelAndView listUser(){
		List<Student> students = studentService.listStudents();
		Map<String, Object> model = new HashMap<String, Object>();
		model.put("students", students);

		return new ModelAndView("usermanagement", "model", model);
	}
	
	@RequestMapping(value ="/createstudent",method = RequestMethod.GET)
	public String createStudentPage(){
		return "createstudent";
	}
	
	@RequestMapping(value ="/createstudent",method = RequestMethod.POST)
	public String createStudent(@RequestParam("id") long id, @RequestParam("name") String name,
			@RequestParam("password") String password, @RequestParam("email") String email){
		
		Student student = new Student();
		student.setId(id);
		student.setName(name);
		student.setEmail(email);
		student.setPassword(password);
		
		studentService.addStudent(student);
		return "redirect:usermanagement";
	}
}
