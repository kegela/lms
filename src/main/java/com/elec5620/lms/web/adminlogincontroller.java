package com.elec5620.lms.web;

import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;
import java.util.UUID;

import javax.annotation.Resource;
import javax.servlet.http.HttpSession;

import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.multipart.MultipartFile;
import org.springframework.web.servlet.ModelAndView;

import com.elec5620.lms.domain.Admin;
import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Teacher;
import com.elec5620.lms.domain.Unit;
import com.elec5620.lms.service.IAdminService;
import com.elec5620.lms.service.IUnitService;
import com.elec5620.lms.util.Base64Util;


/*
 * Login profile - by Leo
 * */
@Controller
public class adminlogincontroller {

	@Resource(name="adminService")
	private IAdminService adminService;
	@Resource(name="unitService")
	private IUnitService unitService;

	
	@RequestMapping(value ="/adminlogin",method = RequestMethod.GET)
	public String logInPage(){
//		Map<String, Object> model = new HashMap<String, Object>();
//        return new ModelAndView("adminlogin", "model", model);
        return "adminlogin";
	}
	
//	@RequestMapping(value="/adminlogin",method=RequestMethod.POST)
//	public ModelAndView adminverifyLogin(){
//		
//			Map<String, Object> model = new HashMap<String, Object>();
//			
////			Student stu=studentService.getStudentById(id);
////			if(stu.getPassword().equals(password))
////			{
////				model.put("studentid", id);
////				
////				Set<Unit> units=unitService.getStudentUnits(id);
////				model.put("name",stu.getName() );
////				model.put("email", stu.getEmail());
////				model.put("major", stu.getMajor());
////				model.put("faculty", stu.getFaculty());
////				model.put("gender", stu.getGender());
////				model.put("studentid", id);
////				model.put("units", units);
////				 return new ModelAndView("studenthomepage", "model", model);
////			}
////			else{
////				 return new ModelAndView("studentlogin", "model", model);
////			}
//			List<Unit> units=unitService.listUnits();
//			model.put("units", units);
//			return new ModelAndView("adminhomepage", "model", model);
//	}	
	
	
	@RequestMapping(value ="/adminlogin",method = RequestMethod.POST)
	public ModelAndView newlogInPage(){
		Map<String, Object> model = new HashMap<String, Object>();
		//model.put("id", id);
		List<Unit> units = unitService.listUnits();
	
		model.put("units", units);
        return new ModelAndView("adminhomepage", "model", model);
	}
	
//	@RequestMapping(value="/adminlogin",method=RequestMethod.POST)
//	public ModelAndView verifyLogin(@RequestParam("id") long id, @RequestParam("password") String password, 
//			HttpSession session, Model model) {
//		Admin admin = AdminService.loginUser(id, password);
//		if(admin == null){
//			model.addAttribute("loginError", "User name and password don't match, please try again!");
//			return new ModelAndView("adminlogin", "model", model);
//		}
//		session.setAttribute("loggedUnUser", admin);
//		return new ModelAndView("redirect:/home", "model", model);
//}
	
	

}
