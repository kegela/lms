package com.elec5620.lms.service;

import java.util.List;
import java.util.Set;

import com.elec5620.lms.domain.Assignment;
import com.elec5620.lms.domain.Material;

import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Teacher;
import com.elec5620.lms.domain.Unit;

public interface IMaterialService {
	public Material getMaterialByid(long materialId);
	public void addMaterial(Material material);
	public void deleteMaterial(long materialId);
	public void updateMaterial(Material material);
	public Set<Material> getMaterials(String unitCode);


}
