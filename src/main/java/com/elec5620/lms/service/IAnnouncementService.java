package com.elec5620.lms.service;

import java.util.List;
import java.util.Set;

import com.elec5620.lms.domain.Announcement;
import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Teacher;
import com.elec5620.lms.domain.Unit;

public interface IAnnouncementService {
	public Announcement getAnnouncementByid(long announcementId);
	public void addAnnouncement(Announcement announcement);
	public void deleteAnnouncement(long announcementId);
	public void updateAnnouncement(Announcement announcement);
	public Set<Announcement> getAnnouncements(String unitCode);
//	public List<Announcement> listTeacherAnnouncements(Teacher teacher);	
//	public List<Announcement> listStudnetAnnouncements(Student student);	
	
}
