package com.elec5620.lms.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.elec5620.lms.DAO.IStudentDAO;
import com.elec5620.lms.domain.Student;

@Service(value="studentService")
public class StudentService implements IStudentService{
	
	@Resource(name="studentDAO")
	private IStudentDAO studentDAO;

	@Override
	public void addStudent(Student student) {
		studentDAO.addStudent(student);
	}
	
	@Override
	public Student getStudentById(long studentid){
		return studentDAO.getStudentById(studentid);
	}

	@Override
	public List<Student> listStudents() {
		// TODO Auto-generated method stub
		return studentDAO.listStudents();
	}

	@Override
	public void updateStudent(Student student) {
		// TODO Auto-generated method stub
		studentDAO.updateStudent(student);
		
	}

	@Override
	public void deleteStudent(long studentId) {
		// TODO Auto-generated method stub
		studentDAO.deleteStudent(studentId);
	}

	@Override
	public void selectUnit(long studentId, String unitCode) {
		// TODO Auto-generated method stub
		studentDAO.selectUnit(studentId, unitCode);
	}

	@Override
	public void dropUnit(long studentId, String unitCode) {
		// TODO Auto-generated method stub
		studentDAO.dropUnit(studentId, unitCode);
	}
	
}
