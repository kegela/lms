package com.elec5620.lms.service;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.elec5620.lms.DAO.IStudentDAO;
import com.elec5620.lms.DAO.ITeacherDAO;
import com.elec5620.lms.DAO.IUnitDAO;
import com.elec5620.lms.domain.Unit;
import com.elec5620.lms.domain.Teacher;
import com.elec5620.lms.domain.Student;

@Service(value="unitService")
public class UnitService implements IUnitService{
	@Resource(name="unitDAO")
	private IUnitDAO unitDAO;
	
	@Resource(name="teacherDAO")
	private ITeacherDAO teacherDAO;
	
	@Resource(name="studentDAO")
	private IStudentDAO studentDAO;
	
	@Override
	public void addUnit(Unit unit) {
		// TODO Auto-generated method stub
		unitDAO.addUnit(unit);
	}

	@Override
	public void updateUnit(Unit unit) {
		// TODO Auto-generated method stub
		unitDAO.update(unit);
		
	}

	@Override
	public void deleteUnit(Unit unit) {
		// TODO Auto-generated method stub
		
		unitDAO.deleteUnit(unit.getCode());
	}

	@Override
	public List<Unit> listUnits() {
		// TODO Auto-generated method stub
		
		return unitDAO.listUnits();
	}

	

	@Override
	public Set<Unit> getTeacherUnits(long teacherId) {
		// TODO Auto-generated method stub
		Teacher teacher=teacherDAO.getTeacherById(teacherId);
		
		return  teacher.getUnits();
	}

	@Override
	public Set<Unit> getStudentUnits(long studentId) {
		// TODO Auto-generated method stub
		Student student=studentDAO.getStudentById(studentId);
		
		return  student.getUnits();
	}

	@Override
	public Unit getUnitByCode(String code) {
		// TODO Auto-generated method stub
		return unitDAO.getUnitByCode(code);
	}

	@Override
	public Set<Student> getUnitStudents(String unitCode) {
		// TODO Auto-generated method stub
		Unit unit=unitDAO.getUnitByCode(unitCode);
		return unit.getStudents();
		
	}

	@Override
	public Set<Teacher> getUnitTeachers(String unitCode) {
		// TODO Auto-generated method stub
		Unit unit=unitDAO.getUnitByCode(unitCode);
		return unit.getTeachers();
	}

}
