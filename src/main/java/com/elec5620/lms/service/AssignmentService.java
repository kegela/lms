package com.elec5620.lms.service;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.elec5620.lms.DAO.IAssignmentDAO;
import com.elec5620.lms.DAO.IMaterialDAO;
import com.elec5620.lms.DAO.IUnitDAO;
import com.elec5620.lms.domain.Assignment;
import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Teacher;
import com.elec5620.lms.domain.Unit;

@Service(value="assignmentService")
public class AssignmentService implements IAssignmentService{

	@Resource(name="assignmentDAO")
	private IAssignmentDAO assignmentDAO;
	
	@Resource(name="unitDAO")
	private IUnitDAO unitDAO;
	
	@Override
	public void addAssignment(Assignment assignment) {
		// TODO Auto-generated method stub
		assignmentDAO.addAssignment(assignment);
		
		
	}

	@Override
	public void deleteAssignment(long assignmentId) {
		// TODO Auto-generated method stub
		assignmentDAO.deleteAssignmentById(assignmentId);
	}

	@Override
	public void updateAssignment(Assignment assignment) {
		// TODO Auto-generated method stub
		assignmentDAO.updateAssignment(assignment);
		
	}

	@Override
	public Set<Assignment> getAssignments(String unitCode) {
		// TODO Auto-generated method stub
		Unit unit=unitDAO.getUnitByCode(unitCode);
		return  unit.getAssignments();
	}

	@Override
	public Assignment getAssignmentByid(long assignmentId) {
		// TODO Auto-generated method stub
		return assignmentDAO.getAssignmentById(assignmentId);
		
	}


	

}
