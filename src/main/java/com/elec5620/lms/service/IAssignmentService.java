package com.elec5620.lms.service;

import java.util.List;
import java.util.Set;

import com.elec5620.lms.domain.Announcement;
import com.elec5620.lms.domain.Assignment;
import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Teacher;
import com.elec5620.lms.domain.Unit;

public interface IAssignmentService {
	public Assignment getAssignmentByid(long assignmentId);
	public void addAssignment(Assignment assignment);
	public void deleteAssignment(long assignmentId);
	public void updateAssignment(Assignment assignment);
	public Set<Assignment> getAssignments(String unitCode);
//	public List<Assignment> listTeacherAssignments(Teacher teacher);	
//	public List<Assignment> listStudnetAssignments(Student student);
}
