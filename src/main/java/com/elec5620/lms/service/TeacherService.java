package com.elec5620.lms.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;


import com.elec5620.lms.DAO.ITeacherDAO;
import com.elec5620.lms.domain.Teacher;

@Service(value="teacherService")

public class TeacherService implements ITeacherService{
	@Resource(name="teacherDAO")
	private ITeacherDAO teacherDAO;

	@Override
	public void addTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		teacherDAO.addTeacher(teacher);
	}

	@Override
	public void updateTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		teacherDAO.updateTeacher(teacher);
	}

	@Override
	public void deleteTeacher(long teacherId) {
		// TODO Auto-generated method stub
		teacherDAO.deleteTeacher(teacherId);
		
		
	}

	@Override
	public List<Teacher> listTeachers() {
		// TODO Auto-generated method stub
		return teacherDAO.listTeachers();
	}
	
	@Override
	public void selectUnit(long teacherId, String unitCode) {
		// TODO Auto-generated method stub
		teacherDAO.selectUnit(teacherId, unitCode);
	}

	@Override
	public void dropUnit(long teacherId, String unitCode) {
		// TODO Auto-generated method stub
		teacherDAO.dropUnit(teacherId, unitCode);
	}

	@Override
	public Teacher getTeacherById(long teacherId) {
		// TODO Auto-generated method stub
		return teacherDAO.getTeacherById(teacherId);
	}
	


}
