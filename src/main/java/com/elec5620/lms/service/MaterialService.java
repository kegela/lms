package com.elec5620.lms.service;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.elec5620.lms.DAO.IMaterialDAO;
import com.elec5620.lms.DAO.IStudentDAO;
import com.elec5620.lms.DAO.IUnitDAO;
import com.elec5620.lms.domain.Material;
import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Teacher;
import com.elec5620.lms.domain.Unit;

@Service(value="materialService")
public class MaterialService implements IMaterialService{

	@Resource(name="materialDAO")
	private IMaterialDAO materialDAO;
	
	@Resource(name="unitDAO")
	private IUnitDAO unitDAO;
	
	@Override
	public void addMaterial(Material material) {
		// TODO Auto-generated method stub
		materialDAO.addMaterial(material);
	}

	@Override
	public void deleteMaterial(long materialId) {
		// TODO Auto-generated method stub
		
		materialDAO.deleteMaterialById(materialId);
	}

	@Override
	public void updateMaterial(Material material) {
		// TODO Auto-generated method stub
		materialDAO.updateMaterial(material);
	}

	@Override
	public Set<Material> getMaterials(String unitCode) {
		// TODO Auto-generated method stub
		Unit unit= unitDAO.getUnitByCode(unitCode);
		
//		for(Material Mtemp:unit.getMaterials())
//		{
//			System.out.println("Material:title="+Mtemp.getTitle());
//			
//		}
		
		return unit.getMaterials();
		
	}

	@Override
	public Material getMaterialByid(long materialId) {
		// TODO Auto-generated method stub
		return materialDAO.getMaterialById(materialId);
		
	}

	

}
