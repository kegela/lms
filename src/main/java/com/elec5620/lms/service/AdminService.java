package com.elec5620.lms.service;

import java.util.List;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.elec5620.lms.DAO.IAdminDAO;
import com.elec5620.lms.DAO.IStudentDAO;
import com.elec5620.lms.domain.Admin;


@Service(value="adminService")
public class AdminService implements IAdminService{

	@Resource(name="adminDAO")
	private IAdminDAO adminDAO;

	@Override
	public Admin getAdminById(long adminId) {
		// TODO Auto-generated method stub
		return adminDAO.getAmintById(adminId);
	}

}
