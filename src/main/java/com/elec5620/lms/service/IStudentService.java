package com.elec5620.lms.service;

import java.util.List;

import com.elec5620.lms.domain.Student;

public interface IStudentService {
	
	public Student getStudentById(long studentid);
	public void addStudent(Student student);
	public void selectUnit(long studentId, String unitCode);
	public void deleteStudent(long studentid);
	public void updateStudent(Student student);
	public List<Student> listStudents();	
	public void dropUnit(long studentId, String unitCode);
}
