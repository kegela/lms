package com.elec5620.lms.service;
import java.util.List;
import java.util.Set;

import com.elec5620.lms.domain.Unit;
import com.elec5620.lms.domain.Teacher;
import com.elec5620.lms.domain.Student;

public interface IUnitService {
	public void addUnit(Unit unit);
	public Unit getUnitByCode(String code);
	public void updateUnit(Unit unit);
	public void deleteUnit(Unit unit);
	public Set<Unit> getTeacherUnits(long teacherId);	
	public Set<Unit> getStudentUnits(long studentId);
	public Set<Student> getUnitStudents(String unitCode);
	public Set<Teacher> getUnitTeachers(String unitCode);
	public List<Unit> listUnits();
}

