package com.elec5620.lms.service;

import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.springframework.stereotype.Service;

import com.elec5620.lms.DAO.IAnnouncementDAO;

import com.elec5620.lms.DAO.IUnitDAO;
import com.elec5620.lms.domain.Announcement;
import com.elec5620.lms.domain.Unit;

@Service(value="announcementService")

public class AnnouncementService implements IAnnouncementService{

	@Resource(name="announcementDAO")
	private IAnnouncementDAO announcementDAO;
	
	@Resource(name="unitDAO")
	private IUnitDAO unitDAO;
	
	@Override
	public void addAnnouncement(Announcement announcement) {
		// TODO Auto-generated method stub
		
		announcementDAO.addAnnouncement(announcement);
		
	}

	@Override
	public void deleteAnnouncement(long announcementId) {
		// TODO Auto-generated method stub
		announcementDAO.deleteAnnouncementById(announcementId);
	}

	@Override
	public void updateAnnouncement(Announcement announcement) {
		// TODO Auto-generated method stub
		announcementDAO.addAnnouncement(announcement);
		
	}

	@Override
	public Set<Announcement> getAnnouncements(String unitCode) {
		// TODO Auto-generated method stub
		Unit unit=unitDAO.getUnitByCode(unitCode);
		return  unit.getAnnouncements();
	}

	@Override
	public Announcement getAnnouncementByid(long announcementId) {
		// TODO Auto-generated method stub
		return announcementDAO.getAnnouncementById(announcementId);
		
	}
	

}
