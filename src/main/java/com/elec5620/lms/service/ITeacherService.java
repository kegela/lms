package com.elec5620.lms.service;

import java.util.List;

import com.elec5620.lms.domain.Teacher;

public interface ITeacherService {
	public void addTeacher(Teacher teacher);
	public void updateTeacher(Teacher teacher);
	public void deleteTeacher(long teacherId);
	public Teacher getTeacherById(long teacherId);
	public List<Teacher> listTeachers();	
	public void selectUnit(long studentId, String unitCode);
	public void dropUnit(long studentId, String unitCode);
}