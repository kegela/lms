package com.elec5620.lms.test;

import java.util.ArrayList;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import javax.annotation.Resource;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;


import com.elec5620.lms.util.SendEmailUtil;

import com.elec5620.lms.DAO.IAnnouncementDAO;
import com.elec5620.lms.DAO.IAssignmentDAO;
import com.elec5620.lms.DAO.IMaterialDAO;
import com.elec5620.lms.DAO.IStudentDAO;
import com.elec5620.lms.DAO.ITeacherDAO;
import com.elec5620.lms.DAO.IUnitDAO;
import com.elec5620.lms.domain.Announcement;
import com.elec5620.lms.domain.Assignment;
import com.elec5620.lms.domain.Material;
import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Teacher;
import com.elec5620.lms.domain.Unit;

import junit.framework.Assert;

@RunWith(SpringJUnit4ClassRunner.class)  
@ContextConfiguration(locations={"file:src/main/webapp/WEB-INF/spring/appServlet/servlet-context.xml","file:src/main/webapp/WEB-INF/spring/appServlet/persistence-context.xml"})
public class DAOTest {
	
	

	@Resource(name="studentDAO")
	private IStudentDAO studentDAO;
	
	@Resource(name="teacherDAO")
	private ITeacherDAO teacherDAO;
	
	@Resource(name="unitDAO")
	private IUnitDAO unitDAO;
	
	@Resource(name="assignmentDAO")
	private IAssignmentDAO assignmentDAO;
	
	@Resource(name="materialDAO")
	private IMaterialDAO materialDAO;
	
	@Resource(name="announcementDAO")
	private IAnnouncementDAO announcementDAO;
	

	
	@Test
	public void testAnnouncement(){
		/* add a Announcement */
//		Announcement announcement = new Announcement();
//		announcement.setTitle("Announcement102");
//		announcement.setDescription("This is Announcement 102.");
//		announcement.setUnit(unitDAO.getUnitByCode("elec5620"));
//		announcementDAO.addAnnouncement(announcement);
		
		/* update a announcement */
//		Announcement announcement = announcementDAO.getAnnouncementById(1);
//		announcement.setTitle("Announcement103");
//		announcementDAO.updateAnnouncement(announcement);
		
		/* get all announcements of one unit */
//		Unit unit = unitDAO.getUnitByCode("elec5620");
//		Assert.assertEquals(2, unit.getAnnouncements().size());
		
		/* delete a announcement */
//		announcementDAO.deleteAnnouncementById(1);
	}
	
	@Test
	public void testMaterial(){
		/* add a material */

//		Material material = new Material();
//		material.setTitle("Material1");
//		material.setUnit(unitDAO.getUnitByCode("ELEC5620"));
//		material.setFilepath("https://s3-ap-southeast-2.amazonaws.com/usyd-lms/Proposal+sample.pdf");
//		materialDAO.addMaterial(material);
		
		/* update a material */
//		Material material = materialDAO.getMaterialById(1);
//		material.setTitle("Material103");
//		materialDAO.updateMaterial(material);
		
		/* get all materials of one unit */
//		Unit unit = unitDAO.getUnitByCode("elec5620");
//		Assert.assertEquals(2, unit.getMaterials().size());
		
		/* delete a material */
//		materialDAO.deleteMaterialById(1);
	}
	
	@Test
	public void testAssignment(){
		/* add a assignment */
//		Assignment assignment = new Assignment();
//		assignment.setTitle("assignment1");
//		assignment.setGrade(80);
//		assignment.setDueDate(new Date());
//		assignment.setDescription("fsdvxcvxcznlckjclk");
//		assignment.setFilepath("https://s3-ap-southeast-2.amazonaws.com/usyd-lms/Project+Requirements.pdf");
//		assignment.setStudent(studentDAO.getStudentById(440555331));
//		assignment.setUnit(unitDAO.getUnitByCode("ELEC5620"));
//		assignmentDAO.addAssignment(assignment);
//		
		/* update a assignment */

//		Assignment assignment = assignmentDAO.getAssignmentById(1);
//		assignment.setGrade(90);

//		assignment.setDescription("assignment4");
//		assignment.setFilepath("https://s3-ap-southeast-2.amazonaws.com/usyd-lms/Project+Requirements.pdf");
//		assignment.setStudent(studentDAO.getStudentById(8880));
//		assignment.setUnit(unitDAO.getUnitByCode("elec5620"));
//		assignmentDAO.addAssignment(assignment);
		
		/* update a assignment */
//		Assignment assignment = assignmentDAO.getAssignmentById(2);
//		assignment.setFilepath("https://s3-ap-southeast-2.amazonaws.com/usyd-lms/Project+Requirements.pdf");

//		Assignment assignment = assignmentDAO.getAssignmentById(2);
//		assignment.setFilepath("https://s3-ap-southeast-2.amazonaws.com/usyd-lms/Project+Requirements.pdf");
//		assignmentDAO.updateAssignment(assignment);
		
		/* get all assignments */
//		Student student = studentDAO.getStudentById(8817);
//		Assert.assertEquals(2, student.getAssignments().size());
		
		/* delete a assignment */
//		assignmentDAO.deleteAssignmentById(1);
	}
	
	
	@Test
    public void testUnit(){	

//		SendEmailUtil send = new SendEmailUtil("sche2145@icloud.com", "Feedme Info", "This message form feedme!");
//		send.sendEmail();

		/* add a unit */
//		Unit unit = new Unit();
//		unit.setCode("COMP9129");
//		unit.setDescription("Software Construction");
//		unit.setName("Software Construction");
//		unitDAO.addUnit(unit);

		/* delete unit */
//    	unitDAO.deleteUnit("elec5620");
   
	}
	
    @Test
    public void testTeacher(){
    	/* add a teacher */
//    	Teacher teacher = new Teacher();
//    	teacher.setId(9991);
//    	teacher.setName("Shiping");
//    	teacherDAO.addTeacher(teacher);
//    	
    	/* select a unit to teacher */
//    	teacherDAO.selectUnit(9991, "elec5620");
    	
    	/* remove a unit form teacher */
//    	teacherDAO.dropUnit(9990, "elec5620");
    	
    	/* list all units & teachers */
//    	Teacher teacher = teacherDAO.getTeacherById(9990);
//    	Assert.assertEquals(1, teacher.getUnits().size());
//    	Unit unit = unitDAO.getUnitByCode("elec5620");
//    	Assert.assertEquals(1, unit.getTeachers().size());
    }	
    
    @Test
    public void testStudent(){
    	/* add a student */
//    	Student student = new Student();
//    	student.setId(440555205);
//    	student.setName("Sen Chen");
//    	student.setEmail("chensen14@gmail.com");
//    	student.setDateofbirth(new Date());
//    	student.setFaculty("IT");
//    	student.setEnrollment_year(2014);
//    	student.setMajor("Software Engineer");
//    	
//    	student.setGender('M');
//    	
//    	studentDAO.addStudent(student);
    	
    	/* select a unit to user */
    //	studentDAO.selectUnit(440555205, "INFO5301");
    	
    	/* remove a unit form user */
//    	studentDAO.dropUnit(440555205, "COMP2007");
    	
    	/* list all units & students */
//    	Student student = studentDAO.getStudentById(8817);
//    	Assert.assertEquals(1, student.getUnits().size());
//    	Unit unit = unitDAO.getUnitByCode("elec5621");
//    	Assert.assertEquals(1, unit.getStudents().size());
    }
}
