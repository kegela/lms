package com.elec5620.lms.DAO;

import java.util.List;

import com.elec5620.lms.domain.Announcement;
import com.elec5620.lms.domain.Material;

public interface IAnnouncementDAO {
	public Announcement getAnnouncementById(long id);
	public void addAnnouncement(Announcement announcement);
	public void updateAnnouncement(Announcement announcement);
	public void deleteAnnouncementById(long id);
	
}
