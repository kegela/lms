package com.elec5620.lms.DAO;

import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Unit;

@Service(value="studentDAO")
@Transactional
public class StudentDAO implements IStudentDAO{

	private SessionFactory sessionFactory;
		
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addStudent(Student student) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(student);
	}

	@Override
	public Student getStudentById(long id) {
		// TODO Auto-generated method stub
		return (Student)sessionFactory.getCurrentSession().get(Student.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Student> listStudents() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("FROM Student").list();
	}

	@Override
	public void updateStudent(Student student) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(student);
	}

	@Override
	public void selectUnit(long studentId, String unitCode) {
		// TODO Auto-generated method stub
		Student student = (Student)sessionFactory.getCurrentSession().get(Student.class, studentId);
		Unit unit = (Unit)sessionFactory.getCurrentSession().get(Unit.class, unitCode);
    	student.getUnits().add(unit);
    	sessionFactory.getCurrentSession().update(student);
	}

	@Override
	public void dropUnit(long studentId, String unitCode) {
		// TODO Auto-generated method stub
		Student student = (Student)sessionFactory.getCurrentSession().get(Student.class, studentId);
		Set<Unit> units = student.getUnits();
    	for(Unit unit : units){
    		if(unit.getCode().equals(unitCode)){
    			units.remove(unit);
    			break;
    		}	
    	}
    	sessionFactory.getCurrentSession().update(student);
	}

	@Override
	public void deleteStudent(long studentId) {
		// TODO Auto-generated method stub
		Student student = new Student();
		student.setId(studentId);
		
		sessionFactory.getCurrentSession().delete(student);
	}
		
}
