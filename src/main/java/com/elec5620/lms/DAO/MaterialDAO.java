package com.elec5620.lms.DAO;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5620.lms.domain.Material;

@Service(value="materialDAO")
@Transactional
public class MaterialDAO implements IMaterialDAO{
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public Material getMaterialById(long id) {
		// TODO Auto-generated method stub
		return (Material)sessionFactory.getCurrentSession().get(Material.class, id);
	}

	@Override
	public void addMaterial(Material material) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(material);
	}

	@Override
	public void updateMaterial(Material material) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(material);
	}

	@Override
	public void deleteMaterialById(long id) {
		// TODO Auto-generated method stub
		Material material = (Material)sessionFactory.getCurrentSession().get(Material.class, id);
		sessionFactory.getCurrentSession().delete(material);
	}
}
