package com.elec5620.lms.DAO;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Unit;

@Service(value="unitDAO")
@Transactional
public class UnitDAO implements IUnitDAO{

	private SessionFactory sessionFactory;
		
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	@Override
	public Unit getUnitByCode(String code) {
		return (Unit)sessionFactory.getCurrentSession().get(Unit.class, code);
	}

	@Override
	public void addUnit(Unit unit) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(unit);
	}

	@Override
	public void deleteUnit(String code) {
		Unit unit = new Unit();
		unit.setCode(code);
		sessionFactory.getCurrentSession().delete(unit);
	}
	


	@SuppressWarnings("unchecked")
	@Override
	public List<Unit> listUnits() {
		return sessionFactory.getCurrentSession().createQuery("FROM Unit").list();
	}
	@Override
	public void update(Unit unit) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(unit);
	}

	
	
	
}
