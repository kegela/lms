package com.elec5620.lms.DAO;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5620.lms.domain.Admin;
import com.elec5620.lms.domain.Announcement;

@Service(value="adminDAO")
@Transactional
public class AdminDAO implements IAdminDAO{
	
	private SessionFactory sessionFactory;
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}
	
	@Override
	public Admin getAmintById(long id) {
		// TODO Auto-generated method stub
		return (Admin)sessionFactory.getCurrentSession().get(Admin.class, id);
		
	}
	

}
