package com.elec5620.lms.DAO;
import java.util.List;

import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Teacher;

public interface ITeacherDAO {

	public void addTeacher(Teacher teacher);
	public void updateTeacher(Teacher teacher);
	public void deleteTeacher(long teacherId);	
	public Teacher getTeacherById(long id);
	public List<Teacher> listTeachers();	
	
	public void selectUnit(long teacherId, String unitCode);
	public void dropUnit(long teacherId, String unitCode);
}
