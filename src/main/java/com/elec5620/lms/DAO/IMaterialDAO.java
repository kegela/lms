package com.elec5620.lms.DAO;

import com.elec5620.lms.domain.Material;

public interface IMaterialDAO {
	public Material getMaterialById(long id);
	public void addMaterial(Material material);
	public void updateMaterial(Material material);
	public void deleteMaterialById(long id);
}
