package com.elec5620.lms.DAO;

import java.util.List;

import com.elec5620.lms.domain.Unit;

public interface IUnitDAO{
	public Unit getUnitByCode(String code);
	public void addUnit(Unit unit);
	public void update(Unit unit);
	public void deleteUnit(String code);
	public List<Unit> listUnits();
}
