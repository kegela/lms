package com.elec5620.lms.DAO;

import java.util.List;

import com.elec5620.lms.domain.Assignment;
import com.elec5620.lms.domain.Unit;

public interface IAssignmentDAO{
	public Assignment getAssignmentById(long id);
	public void addAssignment(Assignment assignment);
	public void updateAssignment(Assignment assignment);
	public void deleteAssignmentById(long id);
}
