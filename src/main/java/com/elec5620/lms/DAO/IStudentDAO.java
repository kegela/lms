package com.elec5620.lms.DAO;

import java.util.List;

import com.elec5620.lms.domain.Student;

public interface IStudentDAO{
	public void selectUnit(long studentId, String unitCode);
	public void dropUnit(long studentId, String unitCode);
	public void addStudent(Student student);
	public void updateStudent(Student student);
	public void deleteStudent(long studentId);
	
	public Student getStudentById(long id);
	// delete student
	public List<Student> listStudents();	
}
