package com.elec5620.lms.DAO;

import java.util.List;

import javax.annotation.Resource;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import com.elec5620.lms.DAO.IAnnouncementDAO;
import com.elec5620.lms.DAO.IStudentDAO;
import com.elec5620.lms.DAO.IUnitDAO;
import com.elec5620.lms.domain.Announcement;
import com.elec5620.lms.domain.Material;
import com.elec5620.lms.domain.Unit;

@Service(value="announcementDAO")
@Transactional
public class AnnouncementDAO implements IAnnouncementDAO{
	private SessionFactory sessionFactory;
	
	
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public Announcement getAnnouncementById(long id) {
		// TODO Auto-generated method stub
		return (Announcement)sessionFactory.getCurrentSession().get(Announcement.class, id);
	}

	@Override
	public void addAnnouncement(Announcement announcement) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(announcement);
	}

	@Override
	public void updateAnnouncement(Announcement announcement) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(announcement);
	}

	@Override
	public void deleteAnnouncementById(long id) {
		// TODO Auto-generated method stub
		Announcement announcement = (Announcement)sessionFactory.getCurrentSession().get(Announcement.class, id);
		sessionFactory.getCurrentSession().delete(announcement);
	}

	

}
