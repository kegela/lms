package com.elec5620.lms.DAO;

import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5620.lms.domain.Assignment;
import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Unit;

@Service(value="assignmentDAO")
@Transactional
public class AssignmentDAO implements IAssignmentDAO{

	private SessionFactory sessionFactory;
		
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public Assignment getAssignmentById(long id) {
		// TODO Auto-generated method stub
		return (Assignment)sessionFactory.getCurrentSession().get(Assignment.class, id);
	}

	@Override
	public void addAssignment(Assignment assignment) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(assignment);
	}

	@Override
	public void deleteAssignmentById(long id) {
		// TODO Auto-generated method stub
		Assignment assignment = (Assignment)sessionFactory.getCurrentSession().get(Assignment.class, id);
		sessionFactory.getCurrentSession().delete(assignment);
	}

	@Override
	public void updateAssignment(Assignment assignment) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(assignment);
	}
	
	
}
