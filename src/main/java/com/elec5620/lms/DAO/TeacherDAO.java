package com.elec5620.lms.DAO;

import java.util.List;
import java.util.Set;

import org.hibernate.SessionFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import com.elec5620.lms.domain.Student;
import com.elec5620.lms.domain.Teacher;
import com.elec5620.lms.domain.Unit;

@Service(value="teacherDAO")
@Transactional
public class TeacherDAO implements ITeacherDAO{
	private SessionFactory sessionFactory;
	
	@Autowired
	public void setSessionFactory(SessionFactory sf) {
		this.sessionFactory = sf;
	}

	@Override
	public void addTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().save(teacher);
	}

	@Override
	public void updateTeacher(Teacher teacher) {
		// TODO Auto-generated method stub
		sessionFactory.getCurrentSession().update(teacher);
	}

	@Override
	public Teacher getTeacherById(long id) {
		// TODO Auto-generated method stub
		return (Teacher)sessionFactory.getCurrentSession().get(Teacher.class, id);
	}

	@SuppressWarnings("unchecked")
	@Override
	public List<Teacher> listTeachers() {
		// TODO Auto-generated method stub
		return sessionFactory.getCurrentSession().createQuery("FROM Teacher").list();
	}
	
	@Override
	public void selectUnit(long teacherId, String unitCode) {
		// TODO Auto-generated method stub
		Teacher teacher = (Teacher)sessionFactory.getCurrentSession().get(Teacher.class, teacherId);
		Unit unit = (Unit)sessionFactory.getCurrentSession().get(Unit.class, unitCode);
		teacher.getUnits().add(unit);
    	sessionFactory.getCurrentSession().update(teacher);
	}

	@Override
	public void dropUnit(long teacherId, String unitCode) {
		// TODO Auto-generated method stub
		Teacher teacher = (Teacher)sessionFactory.getCurrentSession().get(Teacher.class, teacherId);
		Set<Unit> units = teacher.getUnits();
    	for(Unit unit : units){
    		if(unit.getCode().equals(unitCode)){
    			units.remove(unit);
    			break;
    		}	
    	}
    	sessionFactory.getCurrentSession().update(teacher);
	}

	@Override
	public void deleteTeacher(long teacherId) {
		// TODO Auto-generated method stub
		Teacher teacher = new Teacher();
		teacher.setId(teacherId);
		
	}
}
