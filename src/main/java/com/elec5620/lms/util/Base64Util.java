package com.elec5620.lms.util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.Base64;

import org.apache.commons.io.IOUtils;

public class Base64Util {
	public static String toBase64String(File file) throws FileNotFoundException, IOException{
		byte[] imageBytes = IOUtils.toByteArray(new FileInputStream(file));
		String base64 = Base64.getEncoder().encodeToString(imageBytes);
		return base64;
	}
}
