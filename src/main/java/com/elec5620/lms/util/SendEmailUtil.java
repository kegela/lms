package com.elec5620.lms.util;

import java.util.Properties;

import javax.mail.Authenticator;
import javax.mail.Message;
import javax.mail.MessagingException;
import javax.mail.PasswordAuthentication;
import javax.mail.Session;
import javax.mail.Transport;
import javax.mail.internet.InternetAddress;
import javax.mail.internet.MimeMessage;

public class SendEmailUtil {
	private String fromEmailAddress="whiteboardsys@gmail.com";
	private String toEmailAddress;
	private String emailSubject;
	private String emailText;
	
	
    private String host = "smtp.gmail.com";
	
	public SendEmailUtil(String toEmailAddress,String emailSubject,String emailText)
	{
		this.toEmailAddress=toEmailAddress;
		this.emailSubject=emailSubject;
		this.emailText=emailText;

	}
	
	public void sendEmail(){
		// get system properties
		Properties properties = System.getProperties();
		
		  properties.setProperty("mail.transport.protocol", "smtp");     
	      properties.setProperty("mail.host", "smtp.gmail.com");  
	      properties.put("mail.smtp.auth", true);  
	      properties.put("mail.smtp.port", "465");  
	      properties.put("mail.debug", true);  
	      properties.put("mail.smtp.socketFactory.port", "465");  
	      properties.put("mail.smtp.socketFactory.class","javax.net.ssl.SSLSocketFactory");  
	      properties.put("mail.smtp.socketFactory.fallback", false);  
	      
	      properties.setProperty("mail.smtp.host", host);
	      
	      // get session object
	      Session session = Session.getDefaultInstance(properties,new Authenticator(){
	  	    public PasswordAuthentication getPasswordAuthentication()
	  	    {
	  	     return new PasswordAuthentication(fromEmailAddress, "cs88563356"); //发件人邮件用户名、密码
	  	    }
	  	   });
	      try{
	         // create MimeMessage object
	         MimeMessage message = new MimeMessage(session);
	 
	         // Set From
	         message.setFrom(new InternetAddress(fromEmailAddress));
	         // Set TO
	         message.addRecipient(Message.RecipientType.TO,
                     new InternetAddress(toEmailAddress));
	         //Set Subject
	         message.setSubject(emailSubject);
	         //Set Text
	         message.setText(emailText);
	         Transport.send(message);
	         System.out.println("Sent message successfully....");
	         
	      
	      	}catch (MessagingException mex) {
		         mex.printStackTrace();
		    }
	      
	          
	}
	
	
	
	

}
